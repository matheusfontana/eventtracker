﻿using EventTracker.BLL;
using EventTracker.Models;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace EventTracker.Controllers
{
    public class TimeSheetsController : Controller
    {
        private TimeSheetBLL tsBLL = new TimeSheetBLL();

        // GET: TimeSheets
        [Authorize()]
        public ActionResult Index()
        {
            return View(tsBLL.ListAllTimeSheets());
        }

        // GET: TimeSheets/Details/5
        [Authorize()]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSheet timeSheet = tsBLL.Find(id);
            if (timeSheet == null)
            {
                return HttpNotFound();
            }
            return View(timeSheet);
        }

        // GET: TimeSheets/Create
        [Authorize()]
        public ActionResult Create()
        {
            TeamBLL     teamBLL   = new TeamBLL();
            RoleBLL     roleBLL   = new RoleBLL();
            ActivityBLL actBLL    = new ActivityBLL();
            PersonBLL   personBLL = new PersonBLL();

            ViewBag.ActivityId = actBLL.ListAllActivities().Select(p => new SelectListItem
            {
                Text = p.ActivityName + " - " + p.Event.EventName,
                Value = p.ActivityId.ToString()
            });
            ViewBag.PersonId = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text  = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString()
            });
            ViewBag.RoleId     = new SelectList(roleBLL.ListAllRoles(), "RoleId", "RoleName");
            ViewBag.TeamId     = new SelectList(teamBLL.ListAllTeams(), "TeamId", "TeamName");
            return View();
        }

        // POST: TimeSheets/Create    
        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TimeSheetId,PersonId,ActivityId,HoursWorked,TeamId,RoleId,Day")] TimeSheet timeSheet)
        {
            if (ModelState.IsValid)
            {
                tsBLL.Create(timeSheet);
                return RedirectToAction("Index");
            }

            TeamBLL teamBLL = new TeamBLL();
            RoleBLL roleBLL = new RoleBLL();
            ActivityBLL actBLL = new ActivityBLL();
            PersonBLL personBLL = new PersonBLL();

            ViewBag.ActivityId = actBLL.ListAllActivities().Select(p => new SelectListItem
            {
                Text = p.ActivityName + " " + p.Event.EventName,
                Value = p.ActivityId.ToString(),
                Selected = (timeSheet.ActivityId == p.ActivityId) ? true : false
            });

            ViewBag.PersonId = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (timeSheet.PersonId == p.PersonId) ? true : false
            });
            ViewBag.RoleId = new SelectList(roleBLL.ListAllRoles(), "RoleId", "RoleName", timeSheet.RoleId);
            ViewBag.TeamId = new SelectList(teamBLL.ListAllTeams(), "TeamId", "TeamName", timeSheet.TeamId);
            return View(timeSheet);
        }

        // GET: TimeSheets/Edit/5
        [Authorize()]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSheet timeSheet = tsBLL.Find(id);
            if (timeSheet == null)
            {
                return HttpNotFound();
            }
            TeamBLL teamBLL = new TeamBLL();
            RoleBLL roleBLL = new RoleBLL();
            ActivityBLL actBLL = new ActivityBLL();
            PersonBLL personBLL = new PersonBLL();

            ViewBag.ActivityId = actBLL.ListAllActivities().Select(p => new SelectListItem
            {
                Text = p.ActivityName + " " + p.Event.EventName,
                Value = p.ActivityId.ToString(),
                Selected = (timeSheet.ActivityId == p.ActivityId) ? true : false
            });
            ViewBag.PersonId = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (timeSheet.PersonId == p.PersonId) ? true : false
            });
            ViewBag.RoleId = new SelectList(roleBLL.ListAllRoles(), "RoleId", "RoleName", timeSheet.RoleId);
            ViewBag.TeamId = new SelectList(teamBLL.ListAllTeams(), "TeamId", "TeamName", timeSheet.TeamId);
            return View(timeSheet);
        }

        // POST: TimeSheets/Edit/5
        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TimeSheetId,PersonId,ActivityId,HoursWorked,TeamId,RoleId,Day")] TimeSheet timeSheet)
        {
            if (ModelState.IsValid)
            {
                tsBLL.Edit(timeSheet);
                return RedirectToAction("Index");
            }

            TeamBLL teamBLL = new TeamBLL();
            RoleBLL roleBLL = new RoleBLL();
            ActivityBLL actBLL = new ActivityBLL();
            PersonBLL personBLL = new PersonBLL();

            ViewBag.ActivityId = actBLL.ListAllActivities().Select(p => new SelectListItem
            {
                Text = p.ActivityName + " " + p.Event.EventName,
                Value = p.ActivityId.ToString(),
                Selected = (timeSheet.ActivityId == p.ActivityId) ? true : false
            });
            ViewBag.PersonId = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (timeSheet.PersonId == p.PersonId) ? true : false
            });
            ViewBag.RoleId = new SelectList(roleBLL.ListAllRoles(), "RoleId", "RoleName", timeSheet.RoleId);
            ViewBag.TeamId = new SelectList(teamBLL.ListAllTeams(), "TeamId", "TeamName", timeSheet.TeamId);
            return View(timeSheet);
        }

        // GET: TimeSheets/Delete/5
        [Authorize()]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSheet timeSheet = tsBLL.Find(id);
            if (timeSheet == null)
            {
                return HttpNotFound();
            }
            return View(timeSheet);
        }

        // POST: TimeSheets/Delete/5
        [Authorize()]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tsBLL.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
