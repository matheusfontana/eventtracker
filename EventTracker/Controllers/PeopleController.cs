﻿using EventTracker.BLL;
using EventTracker.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace EventTracker.Controllers
{
    public class PeopleController : Controller
    {
        private IPersonBLL personBll;

        public PeopleController()
        {
            personBll = new PersonBLL();
        }

        public PeopleController(IPersonBLL Context)
        {
            personBll = Context;
        }
    
        public ActionResult ThrowError()
        {
            throw new NotImplementedException("This is a sample error"); 
        } 

        // GET: People
        [Authorize()]
        public ActionResult Index()
        {
            List<Person> list = personBll.ListAllPeople();
            if (list == null)
            {
                return HttpNotFound();
            }
            return View("Index", list);
        }

        // GET: People/Details/5
        [Authorize()]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = personBll.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View("Details", person);
        }

        // GET: People/Create
        [Authorize()]
        public ActionResult Create()
        {
            return View("Create");
        }

        // POST: People/Create
        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PersonId,FirstName,LastName")] Person person)
        {
            if (ModelState.IsValid)
            {
                personBll.Create(person);
                return RedirectToAction("Index");
            }

            return View(person);
        }

        // GET: People/Edit/5
        [Authorize()]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = personBll.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View("Edit", person);
        }

        // POST: People/Edit/5
        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PersonId,FirstName,LastName")] Person person)
        {
            if (ModelState.IsValid)
            {
                personBll.Edit(person);
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        [Authorize()]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = personBll.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View("Delete", person);
        }

        // POST: People/Delete/5
        [Authorize()]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            personBll.Delete(id);
            return RedirectToAction("Index");
        }     
    }
}
