﻿using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public interface IPersonBLL
    {
        List<Person> ListAllPeople();
        void Create(Person p);
        Person Find(int? id);
        void Delete(int id);
        void Edit(Person p);
    }
}
