﻿using EventTracker.DAL;
using EventTracker.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventTracker.DTO;

namespace EventTracker.BLL
{
    public class TimeSheetBLL : ITimeSheetBLL
    {

        private ITimeSheetDAL _tsDAL;

        public TimeSheetBLL()
        {
            this._tsDAL = new TimeSheetDAL();
        }

        public List<TimeSheet> ListAllTimeSheets()
        {
            return this._tsDAL.ListAllTimeSheets();
        }

        public void Create(TimeSheet ts)
        {
            this._tsDAL.Create(ts);
        }

        public TimeSheet Find(int? id)
        {
            return this._tsDAL.Find(id);
        }

        public void Delete(int id)
        {
            this._tsDAL.Delete(id);
        }

        public void Edit(TimeSheet ts)
        {
            this._tsDAL.Edit(ts);
        }


        public List<HighestWorkRateDTO> RetrieveHighestWorkRate(string param)
        {
            return this._tsDAL.RetrieveHighestWorkRate(param);

        }
    }
}