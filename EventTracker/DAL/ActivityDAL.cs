﻿using EventTracker.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EventTracker.DAL
{
    public class ActivityDAL : IActivityDAL
    {
        public List<Activity> ListAllActivities()
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                List<Activity> lista = context.Activity.OrderBy(a => a.StartDate)
                                                       .ThenBy(a => a.EventId)
                                                       .Include(e => e.Event)
                                                       .ToList();
                if (lista == null)
                {
                    return new List<Activity>();
                }
                return lista;
            }
        }

        public void Create(Activity a)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                a.CreateDate = DateTime.Now;
                context.Activity.Add(a);
                context.SaveChanges();
            }
        }

        public Activity Find(int? id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Activity act = new Activity();
                if (id != null) 
                {
                    act = context.Activity.Include(e => e.Event).SingleOrDefault(a => a.ActivityId == id);
                }
                return act;
            }
        }

        public void Delete(int id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.Activity.Remove(context.Activity.Find(id));
                context.SaveChanges();
            }
        }

        public void Edit(Activity a)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                a.CreateDate = DateTime.Now;
                context.Entry(a).State = EntityState.Modified;
                context.SaveChanges();
            }
        }


        public List<Activity> ListActiveActivities()
        {
            using (EntitiesContext context = new EntitiesContext()){

                List<Activity> lista = context.Activity.Where(a => DateTime.Compare(a.EndDate, DateTime.Now) > 0
                                                                   && DateTime.Compare(a.StartDate, DateTime.Now) < 0)
                                                       .Include(e => e.Event)
                                                       .OrderBy(a => a.StartDate)
                                                       .Take(5)
                                                       .ToList();
                if (lista == null)
                {
                    return new List<Activity>();
                }
                return lista;
            }
        }


        public List<Activity> ListUpcomingActivities()
        {
            using (EntitiesContext context = new EntitiesContext())
            {

                List<Activity> lista = context.Activity.Where(a => DateTime.Compare(a.StartDate, DateTime.Now) > 0)
                                                       .Include(e => e.Event)
                                                       .OrderBy(a => a.StartDate)
                                                       .Take(5)
                                                       .ToList();
                if (lista == null)
                {
                    return new List<Activity>();
                }
                return lista;
            }
        }


        public void CopyActivitiesFromOtherProject(int from, int to)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                List<Activity> act = context.Activity.Where(a => a.EventId == from).ToList();
                
                foreach (var item in act)
                {
                    Activity activity = new Activity();
                    activity.ActivityName        = item.ActivityName;
                    activity.ActivityDescription = item.ActivityDescription;
                    activity.CreateDate          = DateTime.Now;
                    activity.StartDate           = item.StartDate;
                    activity.EndDate             = item.EndDate;
                    activity.EventId             = to;

                    context.Activity.Add(activity);
                    context.SaveChanges();
                }
            }
        }
    }
}