﻿using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.DAL
{
    public interface IPersonDAL
    {
        List<Person> ListAllPeople();
        void Create(Person p);
        Person Find(int? id);
        void Delete(int id);
        void Edit(Person p);
    }
}
