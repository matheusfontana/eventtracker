﻿using EventTracker.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EventTracker.DAL
{
    public class TeamDAL : ITeamDAL
    {
        public List<Team> ListAllTeams()
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                List<Team> teams = context.Team.OrderBy(t => t.TeamName).Include(p => p.Person).ToList();
                if (teams == null)
                {
                    return new List<Team>();
                }
                return teams;
            }
        }

        public void Create(Team t)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.Team.Add(t);
                context.SaveChanges();
            }
        }

        public Team Find(int? id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Team team = new Team();
                if (id != null)
                {
                    team = context.Team.Include(p => p.Person).SingleOrDefault(t => t.TeamId == id);
                }
                return team;
            }
        }

        public void Delete(int id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Team team = context.Team.Find(id);
                context.Team.Remove(team);
                context.SaveChanges();
            }
        }

        public void Edit(Team t)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.Entry(t).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}