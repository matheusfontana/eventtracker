﻿using EventTracker.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EventTracker.DAL
{
    public class RoleDAL : IRoleDAL
    {

        public List<Role> ListAllRoles()
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                List<Role> roles = context.Role.OrderBy(r => r.RoleName).ToList();
                if (roles == null)
                {
                    return new List<Role>();
                }
                return roles;
            }
        }

        public void Create(Role role)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.Role.Add(role);
                context.SaveChanges();
            }
        }

        public Role Find(int? id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Role role = new Role();
                if (id != null)
                {
                    var roles = context.Role.Where(r => r.RoleId == id);
                    role = roles.Single();
                }
                return role;
            }
        }

        public void Delete(int id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Role role = context.Role.Find(id);
                context.Role.Remove(role);
                context.SaveChanges();
            }
        }

        public void Edit(Role role)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.Entry(role).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}