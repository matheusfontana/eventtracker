﻿using EventTracker.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EventTracker.DAL
{
    public class PersonDAL : IPersonDAL
    {
        public List<Person> ListAllPeople()
        {
            using(EntitiesContext context = new EntitiesContext())
            {
                List<Person> list = context.Person.OrderBy(p => p.FirstName).ToList();
                if(list == null){
                    return new List<Person>();
                }
                return list;
            }
        }


        public void Create(Person person)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.Person.Add(person);
                context.SaveChanges();
            } 
        }


        public Person Find(int? id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Person person = new Person();

                if (id != null)
                {
                    person = context.Person.Find(id);
                }
               return person;
            }
        }

        public void Delete(int id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Person person = context.Person.Find(id);
                context.Person.Remove(person);
                context.SaveChanges();
            }
        }


        public void Edit(Person person)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.Entry(person).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}