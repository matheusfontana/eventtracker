﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventTracker.DTO
{
    public class HighestWorkRateDTO
    {
        public string PersonFullName { get; set; }
        public int TotalHours { get; set; }
        public string TotalHoursFormat { get; set; }
    }
}