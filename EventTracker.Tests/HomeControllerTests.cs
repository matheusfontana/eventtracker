﻿using EventTracker.BLL;
using EventTracker.Controllers;
using EventTracker.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EventTracker.Tests
{
    [TestClass]
    public class HomeControllerTests
    {
        private List<Activity> activities = null;
        private List<Event> events = null;

        [TestInitialize]
        public void Setup()
        {
            //This section of code models an object to be used as sample context data retriveal.
           this.activities = new List<Activity>();
           this.events = new List<Event>();

            activities.Add(new Activity { ActivityId = 0, ActivityName = "Sample0", 
                ActivityDescription = "0", CreateDate = new DateTime(), EndDate = new DateTime(), 
                Event = new Event(), EventId = 0, StartDate = new DateTime(), TimeSheet = new HashSet<TimeSheet>()});
            
            activities.Add(new Activity { ActivityId = 1, ActivityName = "Sample1", 
                ActivityDescription = "1", CreateDate = new DateTime(), EndDate = new DateTime(), 
                Event = new Event(), EventId = 0, StartDate = new DateTime(), TimeSheet = new HashSet<TimeSheet>()});


            events.Add(new Event { EventId = 0, EventName = "Sample0", Description = "0", CreateDate = new DateTime(), 
                EndDate = new DateTime(), EventManagerID = 0, Person = new Person(), StartDate = new DateTime(), 
                Activity = new HashSet<Activity>() });

            events.Add(new Event { EventId = 0, EventName = "Sample1", Description = "1", CreateDate = new DateTime(), 
                EndDate = new DateTime(), EventManagerID = 0, Person = new Person(), StartDate = new DateTime(), 
                Activity = new HashSet<Activity>() });
        }


        [TestMethod]
        public void HomeController_Action_Index_Return_Correct_View()
        {
            //This test checks that the action returns the correct view.
            //Arrange
            Mock<IActivityBLL> actRepo = new Mock<IActivityBLL>();
            Mock<IEventBLL> eventRepo = new Mock<IEventBLL>();

            actRepo.Setup(a => a.ListActiveActivities()).Returns(this.activities);
            actRepo.Setup(a => a.ListUpcomingActivities()).Returns(this.activities);

            eventRepo.Setup(e => e.ListActiveEvents()).Returns(this.events);
            eventRepo.Setup(e => e.ListUpcomingEvents()).Returns(this.events);

            HomeController controller = new HomeController(actRepo.Object, eventRepo.Object);

            //Act
            var result = controller.Index() as ViewResult;

            //Assert
            Assert.AreEqual("Index", result.ViewName); 
        }

        [TestMethod]
        public void HomeController_Action_About_Return_Correct_View()
        {
            HomeController controller = new HomeController();
            var result = controller.About() as ViewResult;
            Assert.AreEqual("About", result.ViewName);
        }

        [TestMethod]
        public void HomeController_Action_Contact_Return_Correct_View()
        {
            HomeController controller = new HomeController();
            var result = controller.Contact() as ViewResult;
            Assert.AreEqual("Contact", result.ViewName);
        }
    }
}
