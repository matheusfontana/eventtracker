﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EventTracker.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using EventTracker.Controllers;
using Moq;
using EventTracker.BLL;


namespace EventTracker.Tests
{
    [TestClass]
    public class EventControllerTests
    {
        private List<Event> events = null;

        [TestInitialize]
        public void Setup()
        {
            this.events = new List<Event>();

            events.Add(new Event
            {
                EventId = 0,
                EventName = "Sample 0",
                CreateDate = new DateTime(),
                EndDate = new DateTime(),
                Description = "sample description 0",
                EventManagerID = 0,
                StartDate = new DateTime()
            });

            events.Add(new Event
            {
                EventId = 1,
                EventName = "Sample 1",
                CreateDate = new DateTime(),
                EndDate = new DateTime(),
                Description = "sample description 1",
                EventManagerID = 0,
                StartDate = new DateTime()
            });

            events.Add(new Event
            {
                EventId = 2,
                EventName = "Sample 2",
                CreateDate = new DateTime(),
                EndDate = new DateTime(),
                Description = "sample description 2",
                EventManagerID = 0,
                StartDate = new DateTime()
            });

        }

        [TestMethod]
        public void EventController_Action_Index_Return_Correct_View()
        {
            //Arrange
            Mock<IEventBLL> eventRepository = new Mock<IEventBLL>();
            eventRepository.Setup(x => x.ListAllEvents()).Returns(this.events);
            EventsController controller = new EventsController(eventRepository.Object);

            //Act
            var result = controller.Index() as ViewResult;

            //Assert
            Assert.AreEqual("Index", result.ViewName);
        }

        //[TestMethod]
        //public void PeopleController_Action_Details_Return_Correct_View()
        //{
        //    //Arrange
        //    Mock<IEventBLL> eventRepository = new Mock<IEventBLL>();
        //    eventRepository.Setup(x => x.Find(1)).Returns(this.events.Find(delegate(Event e) { return e.EventId == 1; }));

        //    EventsController controller = new EventsController(eventRepository.Object);

        //    //Act
        //    var result = controller.Details(1) as ViewResult;

        //    //Assert
        //    Assert.AreEqual("Details", result.ViewName);
        //}

        //[TestMethod]
        //public void PeopleController_Action_Create_Return_Correct_View()
        //{
        //    //Arrange
        //    EventsController controller = new EventsController();

        //    //Act
        //    var result = controller.Create() as ViewResult;

        //    //Assert
        //    Assert.AreEqual("Create", result.ViewName);
        //}

        //[TestMethod]
        //public void PeopleController_Action_Edit_Return_Correct_View()
        //{
        //    //Arrange
        //    Mock<IEventBLL> personRepository = new Mock<IEventBLL>();
        //    personRepository.Setup(x => x.Find(1)).Returns(this.events.Find(delegate(Person p) { return p.PersonId == 1; }));

        //    EventsController controller = new EventsController(personRepository.Object);

        //    //Act
        //    var result = controller.Edit(1) as ViewResult;

        //    //Assert
        //    Assert.AreEqual("Edit", result.ViewName);
        //}

        //[TestMethod]
        //public void PeopleController_Action_Delete_Return_Correct_View()
        //{
        //    //Arrange
        //    Mock<IEventBLL> personRepository = new Mock<IEventBLL>();
        //    personRepository.Setup(x => x.Find(1)).Returns(this.events.Find(delegate(Event e) { return e.EventId == 1; }));

        //    EventsController controller = new EventsController(personRepository.Object);

        //    //Act
        //    var result = controller.Delete(1) as ViewResult;

        //    //Assert
        //    Assert.AreEqual("Delete", result.ViewName);
        //}
    }
}
